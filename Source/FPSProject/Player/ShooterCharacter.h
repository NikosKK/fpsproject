// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

UCLASS()
class FPSPROJECT_API AShooterCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

public:
	
	// spawn inventory, setup initial variables
	virtual void PostInitializeComponents() override;

	//virtual void BeginDestroy() override;

	// cleanup weapons
	virtual void Destroyed() override;

	// update mesh for 1P view
	virtual void PawnClientRestart() override;

	// [server] perform PlayerState related cleanup
	virtual void PossessedBy(class AController* NewController) override;

	// [server] called to determine if we should pause replicate this actor to a specific player
	//virtual bool IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer) override;

	// [client] called when replication is paused for this actor
	//virtual void OnReplicationPausedChanged(bool bPaused) override;

	// [client] perform playerstate related setup
	virtual void OnRep_PlayerState() override;

	/** Update the character. (Running, health etc). */
	virtual void Tick(float DeltaTime) override;

	/** Input *****************************************************/

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// move forwards or backwards
	void MoveForward(float Value);

	// move left/right
	void MoveRight(float Value);

	// move up/down
	void MoveUp(float Value);

	// frame independent turn rate when analog device is used
	void TurnAtRate(float Value);

	// frame independent look rate when analog device is used
	void LookUpAtRate(float Value);

	// player pressed jump button
	void OnStartJump();

	// player released jump button
	void OnEndJump();

	// player pressed start fire action
	void OnStartFire();

	// player released start fire action
	void OnStopFire();

	// player pressed targeting action
	void OnStartTargeting();

	// player released targeting action
	void OnStopTargeting();

	// player pressed reload action
	void OnReload();

	// player chooses previous weapon from inventory
	void OnPrevWeapon();

	// player chooses next weapon from inventory
	void OnNextWeapon();

	/** Weapon ************************************************/

	// [server] add weapon to inventory
	void AddWeapon(class AShooterWeapon* Weapon);

	// [server] removes weapon from the inventory
	void RemoveWeapon(class AShooterWeapon* Weapon);

	class AShooterWeapon* FindWeapon(TSubclassOf<class AShooterWeapon> WeaponClass);

	class AShooterWeapon* GetIndexedWeapon(int32 WeaponIndex) const;

	// [server + local] equips with inventory weapon
	void EquipWeapon(class AShooterWeapon* Weapon);

	// weapon start fire (local)
	void StartWeaponFire();

	// stop weapon fire (local)
	void StopWeaponFire();

	// returns true if the character can fire
	bool CanFire() const;

	// returns true if the character can reload the weapon
	bool CanReload() const;

	// get currently equipped weapon
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	class AShooterWeapon* GetCurrentWeapon() const;

	// returns weapon attach point
	FName GetWeaponAttachPoint() const;

	// return total weapons
	int32 GetWeaponCount() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	FRotator GetAimOffsets() const;

	// get camera view type
	UFUNCTION(BlueprintCallable, Category = "Mesh")
	bool IsFirstPerson() const;

	// get targeting state
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsFiring() const;

	// get targeting state
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	bool IsTargeting() const;

	/** [server + local] change targeting state */
	void SetTargeting(bool bNewTargeting);


	/** camera and animations ********************************/

	//  Add camera pitch to first person mesh.
	void UpdateCamera(const FVector& CameraLocation, const FRotator& CameraRotation);

	/** play anim montage */
	virtual float PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) override;

	/** stop playing montage */
	virtual void StopAnimMontage(class UAnimMontage* AnimMontage) override;

	/** stop playing all montages */
	void StopAllAnimMontages();

	/** Misc ***********************************************/

	/** get mesh component */
	USkeletalMeshComponent* GetPawnMesh() const;

	USkeletalMeshComponent* GetSpecificMesh(bool bFirstPerson);

	// update team color for all player meshes
	void UpdateAllTeamColors();
	
	// check if pawn is still alive
	bool IsAlive() const;

	// returns character max health value
	int32 GetMaxHealth() const;

	// returns health percentage when low health effects should start
	float GetLowHealthPercentage() const;

protected:

	// updates current weapon
	void SetCurrentWeapon(class AShooterWeapon* NewWeapon, class AShooterWeapon* LastWeapon = nullptr);

	/** current weapon rep handler */
	UFUNCTION()
	void OnRep_CurrentWeapon(class AShooterWeapon* LastWeapon);

	// [server] removes and destroys character weapons
	void DetroyWeapons();

	/** handles sounds for running */
	void UpdateRunSounds();

	/** handle mesh visibility and updates */
	void UpdatePawnMeshes();

	/** handle mesh colors on specified material instance */
	void UpdateTeamColors(UMaterialInstanceDynamic* UseMID);

protected:

	UFUNCTION(reliable, server, Withvalidation)
	void ServerEquipWeapon(class AShooterWeapon* NewWeapon);

	/** update targeting state */
	UFUNCTION(reliable, server, WithValidation)
	void ServerSetTargeting(bool bNewTargeting);

public:

	/** health related */
	
	// identifies if the character is notr in the dying state
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	uint32 bIsDead : 1;

	// current pawn health
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "Health")
	float Health;

protected:
	
	// socket or bone name for attaching weapon mesh
	UPROPERTY(EditDefaultsOnly, Category = "Game|Weapon")
	FName WeaponAttachPoint;

	UPROPERTY(Transient, Replicated)
	TArray<class AShooterWeapon*> WeaponList;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentWeapon)
	class AShooterWeapon* CurrentWeapon;

	//UPROPERTY()
	//struct FTakeHitInfo LastTakeHitInfo;

	/** material instances for setting team color in mesh (3rd person view) */
	UPROPERTY(Transient)
	TArray<class UMaterialInstanceDynamic*> MeshMIDs;

	// animation played on death
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	class UAnimMontage* DeathAnim;

	// sound played on death, local player only
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	class USoundCue* DeathSound;

	// effect played on respawn
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	class UParticleSystem* RespawnFX;

	// sound played on respawn
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	class USoundCue* RespawnSound;

	// sound played when health is low
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	class USoundCue* LowHealthSound;

	// sound played when running
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	class USoundCue* RunLoopSound;

	// sound played when stop running
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	class USoundCue* RunStopSound;

	// sound played when targeting state changes
	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	class USoundCue* TargetingSound;

	// used to manipulate with run loop sound
	UPROPERTY()
	class UAudioComponent* RunLoopAC;

	// hook to looped low health sound used to stop/adjust volume
	UPROPERTY()
	class UAudioComponent* LowHealthWarningPlayer;

	// defines when low effects should start
	float LowHealthPercentage;

	// Base turn rate, in deg/sec. Other scaling may affect final turn rate.
	float BaseTurnRate;

	// Base lookup rate, in deg/sec. Other scaling may affect final lookup rate.
	float BaseLookUpRate;

	// current targeting state
	UPROPERTY(Transient, Replicated)
	uint8 bIsTargeting : 1;

	// current firing state
	uint8 bIsFiring : 1;

protected:
	
	/** Returns Mesh1P subobject **/
	FORCEINLINE USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }

private:

	/** pawn mesh: 1st person view */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;
	
};
