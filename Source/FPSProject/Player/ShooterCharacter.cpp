// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterCharacter.h"
#include "ShooterMovementComponent.h"
#include "FPSPlayerController.h"
#include "Weapons/ShooterWeapon.h"
#include "FPSProject.h"

// Sets default values
AShooterCharacter::AShooterCharacter(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer.SetDefaultSubobjectClass<UShooterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// setup FPS skeletal mesh
	Mesh1P = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("FirstPersonMesh"));
	Mesh1P->SetupAttachment(GetCapsuleComponent());
	Mesh1P->bOnlyOwnerSee = true;
	Mesh1P->bOwnerNoSee = false;
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh1P->PrimaryComponentTick.TickGroup = TG_PrePhysics;
	Mesh1P->SetCollisionObjectType(ECC_Pawn);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);

	// setup TPS skeletal mesh
	GetMesh()->bOnlyOwnerSee = false;
	GetMesh()->bOwnerNoSee = true;
	GetMesh()->bReceivesDecals = false;
	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	
	BaseTurnRate = 45.0f;
	BaseLookUpRate = 45.0f;
	Health = 100;

	bIsTargeting = false;
	bIsFiring = false;
}

void AShooterCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (Role == ROLE_Authority)
	{
		Health = GetMaxHealth();
	}

	// set initial visibility (3P view)
	UpdatePawnMeshes();

	// create material instances for team color in 3P view
	for (int32 iMat = 0; iMat < GetMesh()->GetNumMaterials(); iMat++)
	{
		MeshMIDs.Add(GetMesh()->CreateAndSetMaterialInstanceDynamic(iMat));
	}

	// play respawn fx
	if (GetNetMode() != NM_DedicatedServer)
	{
		if (RespawnFX != nullptr)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, RespawnFX, GetActorLocation(), GetActorRotation());
		}

		if (RespawnSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, RespawnSound, GetActorLocation());
		}
	}
}

void AShooterCharacter::Destroyed()
{
	Super::Destroyed();

	// clenup weapon inventory
	DetroyWeapons();
}

void AShooterCharacter::PawnClientRestart()
{
	Super::PawnClientRestart();

	// switch to 1P
	UpdatePawnMeshes();

	// hack!!!
	if (CurrentWeapon == nullptr && WeaponList.Num() > 0)
	{
		CurrentWeapon = WeaponList[0];
	}

	SetCurrentWeapon(CurrentWeapon);

	UMaterialInstanceDynamic* Mesh1PMID = Mesh1P->CreateAndSetMaterialInstanceDynamic(0);
	UpdateTeamColors(Mesh1PMID);
}

void AShooterCharacter::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);

	// [server] as soon as PlayerState is assigned, set team colors for local player
	UpdateAllTeamColors();
}


void AShooterCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	// [client] as soon as PlayerState is assigned, set team colors for local player
	if (PlayerState != nullptr)
	{
		UpdateAllTeamColors();
	}
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/** Input ********************************************************************************************/

void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &AShooterCharacter::MoveUp);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AShooterCharacter::TurnAtRate);

	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AShooterCharacter::LookUpAtRate);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AShooterCharacter::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AShooterCharacter::OnEndJump);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AShooterCharacter::OnStartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AShooterCharacter::OnStopFire);

	PlayerInputComponent->BindAction("Targeting", IE_Pressed, this, &AShooterCharacter::OnStartTargeting);
	PlayerInputComponent->BindAction("Targeting", IE_Released, this, &AShooterCharacter::OnStopTargeting);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AShooterCharacter::OnReload);

	PlayerInputComponent->BindAction("PrevWeapon", IE_Pressed, this, &AShooterCharacter::OnPrevWeapon);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &AShooterCharacter::OnNextWeapon);

}

void AShooterCharacter::MoveForward(float Value)
{
	if (Controller != nullptr && Value != 0)
	{
		const bool bLimitRotation = (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling());
		const FRotator Rotation = bLimitRotation ? GetActorRotation() : Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AShooterCharacter::MoveRight(float Value)
{
	if (Value != 0)
	{
		// get root component rotation
		const FQuat Rotation = GetActorQuat();
		const FVector Direction = FQuatRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void AShooterCharacter::MoveUp(float Value)
{
	// Not when walking or falling.
	if (Value != 0 && (!GetCharacterMovement()->IsMovingOnGround() || !GetCharacterMovement()->IsFalling()))
	{
		AddMovementInput(FVector::UpVector, Value);
	}
}

void AShooterCharacter::TurnAtRate(float Value)
{
	AddControllerYawInput(Value * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookUpAtRate(float Value)
{
	AddControllerPitchInput(Value * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::OnStartJump()
{
	bPressedJump = true;
}

void AShooterCharacter::OnEndJump()
{
	bPressedJump = false;
}

void AShooterCharacter::OnStartFire()
{
	StartWeaponFire();
}

void AShooterCharacter::OnStopFire()
{
	StopWeaponFire();
}

void AShooterCharacter::OnStartTargeting()
{
	AFPSPlayerController* MyPC = Cast<AFPSPlayerController>(Controller);
	if (MyPC)
	{
		SetTargeting(true);
	}
}

void AShooterCharacter::OnStopTargeting()
{
	SetTargeting(false);
}

void AShooterCharacter::OnReload()
{
	if (CurrentWeapon != nullptr)
	{
		CurrentWeapon->StartReload();
	}
}

void AShooterCharacter::OnPrevWeapon()
{
	if (WeaponList.Num() > 1 && (CurrentWeapon == nullptr || CurrentWeapon->GetCurrentState() != EWeaponState::Equipping))
	{
		const int32 WeaponIndex = WeaponList.IndexOfByKey(CurrentWeapon);
		AShooterWeapon* Weapon = WeaponList[(WeaponIndex - 1 + WeaponList.Num()) % WeaponList.Num()];
		EquipWeapon(Weapon);
	}
}

void AShooterCharacter::OnNextWeapon()
{
	if (WeaponList.Num() > 1 && (CurrentWeapon == nullptr || CurrentWeapon->GetCurrentState() != EWeaponState::Equipping))
	{
		const int32 WeaponIndex = WeaponList.IndexOfByKey(CurrentWeapon);
		AShooterWeapon* Weapon = WeaponList[(WeaponIndex + 1) % WeaponList.Num()];
		EquipWeapon(Weapon);
	}

}

//******************************************************************************************
void AShooterCharacter::AddWeapon(AShooterWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		// add the weapon to inventory
		Weapon->AddWeapon(this);
		WeaponList.AddUnique(Weapon);

		// equip first inventory weapon
		if (CurrentWeapon == nullptr && WeaponList.Num() > 0)
		{
			EquipWeapon(WeaponList[0]);
		}
	}
}

void AShooterCharacter::RemoveWeapon(AShooterWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		Weapon->RemoveWeapon();
		WeaponList.RemoveSingle(Weapon);
	}
}

AShooterWeapon* AShooterCharacter::FindWeapon(TSubclassOf<class AShooterWeapon> WeaponClass)
{
	for (int32 i = 0; i < WeaponList.Num(); i++)
	{
		AShooterWeapon* Weapon = WeaponList[i];
		if (Weapon && Weapon->IsA(WeaponClass))
		{
			return Weapon;
		}
	}

	return nullptr;
}

AShooterWeapon * AShooterCharacter::GetIndexedWeapon(int32 WeaponIndex) const
{
	if (WeaponList.IsValidIndex(WeaponIndex))
	{
		return WeaponList[WeaponIndex];
	}

	return nullptr;
}

AShooterWeapon* AShooterCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

void AShooterCharacter::EquipWeapon(AShooterWeapon*  Weapon)
{
	if (Weapon)
	{
		if (Role == ROLE_Authority)
		{
			SetCurrentWeapon(Weapon, CurrentWeapon);
		}
		else
		{
			ServerEquipWeapon(Weapon);
		}
	}
}

bool AShooterCharacter::ServerEquipWeapon_Validate(AShooterWeapon* NewWeapon)
{
	return true;
}

void AShooterCharacter::ServerEquipWeapon_Implementation(AShooterWeapon* NewWeapon)
{
	EquipWeapon(NewWeapon);
}

void AShooterCharacter::StartWeaponFire()
{
	if (!bIsFiring)
	{
		bIsFiring = true;
		if (CurrentWeapon != nullptr)
		{
			CurrentWeapon->StartFire();
		}
	}
}

void AShooterCharacter::StopWeaponFire()
{
	if (bIsFiring)
	{
		bIsFiring = false;
		if (CurrentWeapon != nullptr)
		{
			CurrentWeapon->StopFire();
		}
	}
}

bool AShooterCharacter::CanFire() const
{
	return IsAlive();
}

bool AShooterCharacter::CanReload() const
{
	return true;
}

FName AShooterCharacter::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}

int32 AShooterCharacter::GetWeaponCount() const
{
	return WeaponList.Num();
}

bool AShooterCharacter::ServerSetTargeting_Validate(bool bNewTargeting)
{
	return true;
}

void AShooterCharacter::ServerSetTargeting_Implementation(bool bNewTargeting)
{
	SetTargeting(bNewTargeting);
}

void AShooterCharacter::SetTargeting(bool bNewTargeting)
{
	bIsTargeting = bNewTargeting;

	if (TargetingSound)
	{
		UGameplayStatics::SpawnSoundAttached(TargetingSound, GetRootComponent());
	}

	if (Role < ROLE_Authority)
	{
		ServerSetTargeting(bNewTargeting);
	}
}

//*****************************************************************************************************

void AShooterCharacter::UpdateCamera(const FVector& CameraLocation, const FRotator& CameraRotation)
{
	USkeletalMeshComponent* DefaultMesh1P = Cast<USkeletalMeshComponent>(GetClass()->GetDefaultSubobjectByName(TEXT("FirstPersonMesh")));
	const FMatrix DefMeshLS = FRotationTranslationMatrix(DefaultMesh1P->RelativeRotation, DefaultMesh1P->RelativeLocation);
	const FMatrix LocalToWorld = ActorToWorld().ToMatrixWithScale();

	// Mesh rotating code expect uniform scale in LocalToWorld matrix
	const FRotator RotCameraPitch(CameraRotation.Pitch, 0.0f, 0.0f);
	const FRotator RotCameraYaw(0.0f, CameraRotation.Yaw, 0.0f);

	const FMatrix LeveledCameraLS = FRotationTranslationMatrix(RotCameraYaw, CameraLocation) * LocalToWorld.Inverse();
	const FMatrix PitchedCameraLS = FRotationMatrix(RotCameraPitch) * LeveledCameraLS;
	const FMatrix MeshRelativeToCamera = DefMeshLS * LeveledCameraLS.Inverse();
	const FMatrix PitchedMesh = MeshRelativeToCamera * PitchedCameraLS;

	Mesh1P->SetRelativeLocationAndRotation(PitchedMesh.GetOrigin(), PitchedMesh.Rotator());
}

float AShooterCharacter::PlayAnimMontage(UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	USkeletalMeshComponent* SkelMesh = GetPawnMesh();
	if (AnimMontage && SkelMesh && SkelMesh->AnimScriptInstance)
	{
		return SkelMesh->AnimScriptInstance->Montage_Play(AnimMontage, InPlayRate);
	}
	
	return 0.0f;
}

void AShooterCharacter::StopAnimMontage(UAnimMontage* AnimMontage)
{
	USkeletalMeshComponent* SkelMesh = GetPawnMesh();
	if (AnimMontage && SkelMesh && SkelMesh->AnimScriptInstance && SkelMesh->AnimScriptInstance->Montage_IsPlaying(AnimMontage))
	{
		SkelMesh->AnimScriptInstance->Montage_Stop(AnimMontage->BlendOut.GetBlendTime(), AnimMontage);
	}
}

void AShooterCharacter::StopAllAnimMontages()
{
	USkeletalMeshComponent* SkelMesh = GetPawnMesh();
	if (SkelMesh && SkelMesh->AnimScriptInstance)
	{
		SkelMesh->AnimScriptInstance->Montage_Stop(0.0f);
	}
}

USkeletalMeshComponent* AShooterCharacter::GetPawnMesh() const
{
	return (IsFirstPerson() ? Mesh1P : GetMesh());
}

USkeletalMeshComponent * AShooterCharacter::GetSpecificMesh(bool bFirstPerson)
{
	return (bFirstPerson ? Mesh1P : GetMesh());
}

void AShooterCharacter::UpdateAllTeamColors()
{
	for (int32 i = 0; i < MeshMIDs.Num(); i++)
	{
		UpdateTeamColors(MeshMIDs[i]);
	}
}

FRotator AShooterCharacter::GetAimOffsets() const
{
	const FVector AimDirWS = GetBaseAimRotation().Vector();
	const FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirWS);
	const FRotator AimRotLS = AimDirLS.Rotation();

	return AimRotLS;
}

bool AShooterCharacter::IsFirstPerson() const
{
	return (!bIsDead && Controller != nullptr && Controller->IsLocalPlayerController());
}

bool AShooterCharacter::IsFiring() const
{
	return bIsFiring;
}

bool AShooterCharacter::IsTargeting() const
{
	return bIsTargeting;
}

bool AShooterCharacter::IsAlive() const
{
	return Health > 0;
}

int32 AShooterCharacter::GetMaxHealth() const
{
	return GetClass()->GetDefaultObject<AShooterCharacter>()->Health;
}

float AShooterCharacter::GetLowHealthPercentage() const
{
	return LowHealthPercentage;
}

void AShooterCharacter::SetCurrentWeapon(AShooterWeapon* NewWeapon, AShooterWeapon* LastWeapon)
{
	AShooterWeapon* LocalLastWeapon = nullptr;

	if (LastWeapon != nullptr)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	if (LocalLastWeapon)
	{
		LocalLastWeapon->UnEquip();
	}

	CurrentWeapon = NewWeapon;

	if (NewWeapon)
	{
		// Make sure weapon's MyPawn is pointing back to us.
		// During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!
		NewWeapon->SetPawnOwner(this);

		NewWeapon->Equip(LastWeapon);

		UE_LOG(LogTemp, Log, TEXT("Equipped with new weapon"));
	}
}

void AShooterCharacter::OnRep_CurrentWeapon(AShooterWeapon* LastWeapon)
{
	SetCurrentWeapon(CurrentWeapon, LastWeapon);
}

void AShooterCharacter::DetroyWeapons()
{
	if (Role < ROLE_Authority)
	{
		return;
	}

	for (int32 i = WeaponList.Num() - 1; i >= 0; i--)
	{
		AShooterWeapon* Weapon = WeaponList[i];
		if (Weapon)
		{
			RemoveWeapon(Weapon);
			Weapon->Destroy();
		}
	}
}

void AShooterCharacter::UpdatePawnMeshes()
{
	Mesh1P->MeshComponentUpdateFlag = !IsFirstPerson() ? EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered : EMeshComponentUpdateFlag::AlwaysTickPose;
	Mesh1P->SetOwnerNoSee(!IsFirstPerson());

	GetMesh()->MeshComponentUpdateFlag = IsFirstPerson() ? EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered : EMeshComponentUpdateFlag::AlwaysTickPose;
	GetMesh()->SetOwnerNoSee(IsFirstPerson());
}

void AShooterCharacter::UpdateTeamColors(UMaterialInstanceDynamic * UseMID)
{
	UE_LOG(LogTemp, Warning, TEXT("Materials updated!"));
}

/** Replication *****************************************************************************/

void AShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// only to local owner: weapon change requests are locally instigated, other clients don't need it
	DOREPLIFETIME_CONDITION(AShooterCharacter, WeaponList, COND_OwnerOnly);

	// everyone except local owner: flag change is locally instigated
	DOREPLIFETIME_CONDITION(AShooterCharacter, bIsTargeting, COND_SkipOwner);
	//DOREPLIFETIME_CONDITION(AShooterCharacter, bWantsToRun, COND_SkipOwner);

	//DOREPLIFETIME_CONDITION(AShooterCharacter, LastTakeHitInfo, COND_Custom);

	// everyone
	DOREPLIFETIME(AShooterCharacter, CurrentWeapon);
	DOREPLIFETIME(AShooterCharacter, Health);
}

