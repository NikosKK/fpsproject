// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProjectGameModeBase.h"
#include "Player/FPSPlayerController.h"
#include "Player/ShooterCharacter.h"
#include "FPSProject.h"

AFPSProjectGameModeBase::AFPSProjectGameModeBase()
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnObject(TEXT("/Game/Blueprints/Pawns/PlayerPawn"));
	DefaultPawnClass = PlayerPawnObject.Class;


	PlayerControllerClass = AFPSPlayerController::StaticClass();

}