// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShooterWeapon.generated.h"

namespace EWeaponState
{
	enum Type
	{
		Idle,
		Firing,
		Reloading,
		Equipping,
	};
}

USTRUCT()
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

	/** inifite ammo for reloads */
	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	bool bInfiniteAmmo;

	/** infinite ammo in clip, no reload required */
	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	bool bInfiniteClip;

	/** max ammo */
	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 MaxAmmo;

	/** clip size */
	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 AmmoPerClip;

	/** initial clips */
	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 InitialClips;

	/** time between two consecutive shots */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float TimeBetweenShots;

	/** failsafe reload duration if weapon doesn't have any animation for it */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	float NoAnimReloadDuration;

	/** defaults */
	FWeaponData()
	{
		bInfiniteAmmo = false;
		bInfiniteClip = false;
		MaxAmmo = 100;
		AmmoPerClip = 20;
		InitialClips = 4;
		TimeBetweenShots = 0.2f;
		NoAnimReloadDuration = 1.0f;
	}
};

USTRUCT()
struct FWeaponAnim
{
	GENERATED_USTRUCT_BODY()

	/** animation played on pawn (1st person view) */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	class UAnimMontage* Pawn1P;

	/** animation played on pawn (3rd person view) */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	class UAnimMontage* Pawn3P;
};

UCLASS(Abstract, Blueprintable)
class FPSPROJECT_API AShooterWeapon : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AShooterWeapon();

	virtual void PostInitializeComponents() override;

	virtual void Destroyed() override;

public:

	enum class EAmmoType
	{
		EBullet,
		ERocket,
		EMax,
	};

	/** Equip weapon ********************************************/
	
	// owner pawn is equipped by this weapon 
	virtual void Equip(const AShooterWeapon* LastWeapon);

	virtual void OnEquiped();

	// weapon is unmounted by the owner pawn
	virtual void UnEquip();

	// [server] weapon added to the pawn's weapons
	virtual void AddWeapon(class AShooterCharacter* NewOwner);

	// [server] weapon removed from pawn's weapons
	virtual void RemoveWeapon();

	// check if currently equipped
	bool IsEquipped() const;

	// check if the mesh is attached to the owner
	bool IsAttached() const;

	// returns the time this weapon has been equipped
	float GetEquippedStartTime() const;

	// returns the duration this weapon is equipped
	float GetEquippedDuration() const;

	/** Input ********************************************/

	// [local + server] start weapon fire
	virtual void StartFire();

	// [local + server] stop weapon fire
	virtual void StopFire();

	// [all] start weapon reload
	virtual void StartReload(bool bFromReplication = false);

	// [local + server] interrupt weapon reload
	virtual void StopReload();

	// [server] performs actual reload
	virtual void ReloadWeapon();

	// server trigger reload
	UFUNCTION(reliable, client)
	void ClientStartReload();

	// check if the weapon can fire
	bool CanFire() const;

	// check if the weapon can be reloaded
	bool CanReload() const;

	/** Weapon status and stats ***********************/

	// returns weapon status
	EWeaponState::Type GetCurrentState() const;

	// [server] add ammo
	void GiveAmmo(int Amount);

	// consume a bullet
	void UseAmmo();

	// returns current ammo
	int32 GetCurrentAmmo() const;

	// returns current clip ammo
	int32 GetCurrentClipAmmo() const;

	// returns clip size
	int32 GetClipSize() const;

	// returns max ammo amount
	int32 GetMaxAmmo() const;

	// check if the weapon has infinite ammo (including owner cheats!)
	bool HasInfiniteAmmo() const;

	// check if the weapon has infinite clips (including owner cheats!)
	bool HasInfiniteClip() const;

	// returns the skeletal mesh used for this weapon
	// needs pawn owner to determine variant
	USkeletalMeshComponent* GetWeaponMesh() const;

	// returns the pawn owner for this weapon
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	class AShooterCharacter* GetPawnOwner() const;

	void SetPawnOwner(class AShooterCharacter* NewOwner);

	// attach weapon mesh to pawn mesh
	void AttachWeaponToPawn();

	// detaches weapon mesh from pawn mesh
	void DetachWeaponFromPawn();

protected:

	// update weapon state
	void SetWeaponState(EWeaponState::Type NewState);

	// find best state for current state
	void DetermineWeaponState();

	/** Input ********************************************/

	UFUNCTION(reliable, server, WithValidation)
	void ServerStartFire();
	bool ServerStartFire_Validate();
	void ServerStartFire_Implementation();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStopFire();
	bool ServerStopFire_Validate();
	void ServerStopFire_Implementation();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStartReload();
	bool ServerStartReload_Validate();
	void ServerStartReload_Implementation();

	UFUNCTION(reliable, server, WithValidation)
	void ServerStopReload();
	bool ServerStopReload_Validate();
	void ServerStopReload_Implementation();

	// [server] fire and update ammo
	UFUNCTION(reliable, server, WithValidation)
	void ServerHandleFiring();
	bool ServerHandleFiring_Validate();
	void ServerHandleFiring_Implementation();

	/** Weapon usage */
	// query ammo type
	virtual EAmmoType GetAmmoType() const { return EAmmoType::EBullet; }

	// [local] specific weapon fire implementation
	virtual void FireWeapon() PURE_VIRTUAL(AShooterWeapon::FireWeapon, );

	// [local + server] fire and update ammo
	void HandleFiring();

	// [local + server] firing started
	virtual void OnBurstStarted();

	// [local + server] firing finished
	virtual void OnBurstFinished();

	// called in network play to start the cosmetic fx
	virtual void StartFireFX();

	// called in network play to stop the cosmetic fx
	virtual void StopFireFX();

	/** Helpers ******************************************/

	// plays weapon sounds
	class UAudioComponent* PlayWeaponSound(class USoundCue* Sound);

	// plays weapon animations
	float PlayWeaponAnimation(const FWeaponAnim& Animation);

	// stops weapon animation
	void StopWeaponAnimation(const FWeaponAnim& Animation);

	// get the weapon aim, allowing for adjustments to be made by the weapon
	virtual FVector GetAdjustedAim() const;

	// returns the camera aim
	FVector GetCameraAim() const;

	// returns the originating location for camera damage
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	// returns the weapon's muzzle location
	FVector GetMuzzleLocation() const;

	// returns the weapon's muzzle direction
	FVector GetMuzzleDirection() const;

	// returns the hit result between start and end locations
	FHitResult GetWeaponHit(const FVector& StartTrace, const FVector& EndTrace) const;

	/** Replication ******************************************/

	UFUNCTION()
	void OnRep_PawnOwner();

	UFUNCTION()
	void OnRep_BurstCounter();

	UFUNCTION()
	void OnRep_Reload();

protected:

	// pawn owner
	UPROPERTY(Transient, ReplicatedUsing = OnRep_PawnOwner)
	class AShooterCharacter* PawnOwner;

	// weapon configuration data
	UPROPERTY(EditDefaultsOnly, Category = "Config")
	FWeaponData WeaponConfig;

	// firing audio (bLoopedFireSound set) */
	UPROPERTY(Transient)
	class UAudioComponent* FireAC;

	// name of bone/socket for muzzle in weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName MuzzleAttachPoint;

	// FX for muzzle flash */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* MuzzleFX;

	// spawned component for muzzle FX */
	UPROPERTY(Transient)
	class UParticleSystemComponent* MuzzlePSC;

	// spawned component for second muzzle FX (Needed for split screen) */
	UPROPERTY(Transient)
	class UParticleSystemComponent* MuzzlePSCSecondary;

	// camera shake on firing */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<class UCameraShake> FireCameraShake;

	// force feedback effect to play when the weapon is fired */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	class UForceFeedbackEffect* FireForceFeedback;

	// single fire sound (bLoopedFireSound not set)
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	class USoundCue* FireSound;

	// looped fire sound (bLoopedFireSound set)
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	class USoundCue* FireLoopSound;

	// finished burst sound (bLoopedFireSound set)
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	class USoundCue* FireFinishSound;

	// out of ammo sound
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	class USoundCue* OutOfAmmoSound;

	// reload sound
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	class USoundCue* ReloadSound;
	
	// equip sound
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* EquipSound;

	// reload animations
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	FWeaponAnim ReloadAnim;

	// equip animations
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	FWeaponAnim EquipAnim;

	// fire animations
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	FWeaponAnim FireAnim;

	// is muzzle FX looped?
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	uint32 bLoopedMuzzleFX : 1;

	// is fire sound looped?
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	uint32 bLoopedFireSound : 1;

	// is fire animation looped?
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	uint32 bLoopedFireAnim : 1;

	// is fire animation playing?
	uint32 bPlayingFireAnim : 1;

	// is weapon currently equipped
	uint32 bIsEquipped : 1;

	// is weapon fire active?
	uint32 bWantsToFire : 1;

	// is reload animation playing?
	UPROPERTY(Transient, ReplicatedUsing = OnRep_Reload)
	uint32 bPendingReload : 1;

	// is equip animation playing?
	uint32 bPendingEquip : 1;

	// weapon is refiring
	uint32 bRefiring;

	// current weapon state
	EWeaponState::Type CurrentState;

	// time of last successful weapon fire
	float LastFireTime;

	// last time when this weapon was switched to
	float EquipStartedTime;

	// how much time weapon needs to be equipped
	float EquipDuration;

	// current total ammo
	UPROPERTY(Transient, Replicated)
	int32 CurrentAmmo;

	// current ammo - inside clip
	UPROPERTY(Transient, Replicated)
	int32 CurrentAmmoInClip;

	// burst counter, used for replicating fire events to remote clients
	UPROPERTY(Transient, ReplicatedUsing = OnRep_BurstCounter)
	int32 BurstCounter;

	// Handle for efficient management of OnEquipFinished timer
	FTimerHandle TimerHandle_OnEquipFinished;

	// Handle for efficient management of StopReload timer
	FTimerHandle TimerHandle_StopReload;

	// Handle for efficient management of ReloadWeapon timer
	FTimerHandle TimerHandle_ReloadWeapon;

	// Handle for efficient management of HandleFiring timer
	FTimerHandle TimerHandle_HandleFiring;

private:

	UPROPERTY(VisibleDefaultsOnly, Category = "Mesh" )
	USkeletalMeshComponent* Mesh1P;

	UPROPERTY(VisibleDefaultsOnly, Category = "Mesh")
	USkeletalMeshComponent* Mesh3P;

protected:
	/** Returns Mesh1P subobject **/
	FORCEINLINE USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns Mesh3P subobject **/
	FORCEINLINE USkeletalMeshComponent* GetMesh3P() const { return Mesh3P; }
	
};
