// Fill out your copyright notice in the Description page of Project Settings.

#include "InstantHitImpactEffect.h"
#include "FPSProject.h"

// Sets default values
AInstantHitImpactEffect::AInstantHitImpactEffect()
{
	bAutoDestroyWhenFinished = true;
}

// Called when the game starts or when spawned
void AInstantHitImpactEffect::BeginPlay()
{
	Super::BeginPlay();

	UPhysicalMaterial* HitPhysicalMaterial = SurfaceHit.PhysMaterial.Get();
	EPhysicalSurface HitsurfaceType = UPhysicalMaterial::DetermineSurfaceType(HitPhysicalMaterial);

	// play impact effect
	UParticleSystem* ImpactFX = GetImpactFX(HitsurfaceType);
	if (ImpactFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, ImpactFX, GetActorLocation(), GetActorRotation());
	}

	// play impact sound 
	USoundCue* ImpactSound = GetImpactSound(HitsurfaceType);
	if (ImpactSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, GetActorLocation());
	}

	if (DefaultDecal.DecalMaterial)
	{
		FRotator RandomDecalRotation = SurfaceHit.ImpactNormal.Rotation();
		RandomDecalRotation.Roll = FMath::FRandRange(-180.0f, 180.0f);

		UGameplayStatics::SpawnDecalAttached(DefaultDecal.DecalMaterial, FVector(1.0f, DefaultDecal.DecalSize, DefaultDecal.DecalSize),
			SurfaceHit.GetComponent(), SurfaceHit.BoneName,
			SurfaceHit.ImpactPoint, RandomDecalRotation, EAttachLocation::KeepWorldPosition,
			DefaultDecal.LifeSpan);
	}

}

UParticleSystem* AInstantHitImpactEffect::GetImpactFX(TEnumAsByte<EPhysicalSurface> SurfaceType) const
{
	UParticleSystem* ImpactFX = nullptr;

	switch (SurfaceType)
	{
		case FPSPROJECT_SURFACE_Concrete:
			ImpactFX = ConcreteFX;
			break;

		default:
			ImpactFX = DefaultFX;
			break;
	}

	return ImpactFX;
}

USoundCue * AInstantHitImpactEffect::GetImpactSound(TEnumAsByte<EPhysicalSurface> SurfaceType) const
{
	USoundCue* ImpactSound = nullptr;

	switch (SurfaceType)
	{
		case FPSPROJECT_SURFACE_Concrete:
			ImpactSound = ConcreteSound;
			break;

		default:
			ImpactSound = DefaultSound;
			break;
	}

	return ImpactSound;

}
