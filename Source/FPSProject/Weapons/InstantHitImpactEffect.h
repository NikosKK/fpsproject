// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSGameTypes.h"
#include "InstantHitImpactEffect.generated.h"

/*
 * Spawnable effect for weapon hit impact - NOT replicated to clients
 * Each impact type should be defined as separate blueprint
 */
UCLASS(Abstract, Blueprintable)
class FPSPROJECT_API AInstantHitImpactEffect : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInstantHitImpactEffect();

protected:

	virtual void BeginPlay() override;

	// get impact effects for the specified material type
	class UParticleSystem* GetImpactFX(TEnumAsByte<EPhysicalSurface> SurfaceType) const;

	// get sound effect for the specified material type
	class USoundCue* GetImpactSound(TEnumAsByte<EPhysicalSurface> SurfaceType) const;

public:

	// default impact fx when material specific override does not exist
	UPROPERTY(EditDefaultsOnly, Category = "Defaults")
	class UParticleSystem* DefaultFX;

	UPROPERTY(EditDefaultsOnly, Category = "Defaults")
	class USoundCue* DefaultSound;

	UPROPERTY(EditDefaultsOnly, Category = "Defaults")
	struct FDecalData DefaultDecal;

	UPROPERTY(BlueprintReadonly, Category = "Surface")
	FHitResult SurfaceHit;

	// impact FX on concrete
	UPROPERTY(EditDefaultsOnly, Category = "SurfaceFX")
	class UParticleSystem* ConcreteFX;

	// impact sound on concrete
	UPROPERTY(EditDefaultsOnly, Category = "Default")
	class USoundCue* ConcreteSound;

};
