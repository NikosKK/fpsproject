// Fill out your copyright notice in the Description page of Project Settings.

#include "InstantHitWeapon.h"
#include "InstantHitImpactEffect.h"

#include "Player/ShooterCharacter.h"

#include "FPSProject.h"

AInstantHitWeapon::AInstantHitWeapon()
{
	CurrentFiringSpread = 0;
}

void AInstantHitWeapon::FireWeapon()
{
	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpead = GetCurrentSpread();
	const float ConeHalfAngle = FMath::DegreesToRadians(CurrentSpead * 0.5f);

	const FVector AimDir = GetAdjustedAim();
	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	//const FVector StartTrace = GetMuzzleLocation();
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
	const FVector EndTrace = StartTrace + ShootDir * InstantWeaponConfig.WeaponRange;

	const FHitResult Hit = GetWeaponHit(StartTrace, EndTrace);
	HandleInstantHit(Hit, StartTrace, ShootDir, RandomSeed, CurrentSpead);

	// update firing spread
	CurrentFiringSpread = FMath::Min(InstantWeaponConfig.FiringMaxSpreadIncrement,
									 InstantWeaponConfig.FiringSpreadIncrement + CurrentSpead);
}

void AInstantHitWeapon::OnBurstFinished()
{
	Super::OnBurstFinished();

	CurrentFiringSpread = 0.0f;
}

void AInstantHitWeapon::HandleInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread)
{
	if (PawnOwner != nullptr && PawnOwner->IsLocallyControlled() && GetNetMode() == NM_Client)
	{
		// if we hit something that is controlled by the server, notify the server
		if (Impact.GetActor() && Impact.GetActor()->GetRemoteRole() == ROLE_Authority)
		{
			ServerNotifyHit(Impact, ShootDir, RandomSeed, ReticleSpread);
		}
	}
	else if (Impact.GetActor() == nullptr)
	{
		if (Impact.bBlockingHit)
		{
			ServerNotifyHit(Impact, ShootDir, RandomSeed, ReticleSpread);
		}
		else
		{
			ServerNotifyMiss(ShootDir, RandomSeed, ReticleSpread);
		}
	}
}

void AInstantHitWeapon::ProcessInstantHit(const FHitResult & Impact, const FVector & Origin, const FVector & ShootDir, int32 RandomSeed, float ReticleSpread)
{
	if (ShouldProcessDamage(Impact.GetActor()))
	{
		ProcessDamage(Impact, ShootDir);
	}

	// play FX on remote clients
	if (Role == ROLE_Authority)
	{
		HitNotification.Origin = Origin;
		HitNotification.RandomSeed = RandomSeed;
		HitNotification.ReticleSpread = ReticleSpread;
	}

	// play FX locally
	if (GetNetMode() != NM_DedicatedServer)
	{
		const FVector EndTrace = Origin + ShootDir * InstantWeaponConfig.WeaponRange;
		const FVector EndPoint = Impact.GetActor() ? Impact.ImpactPoint : EndTrace;

		// show bullet trail effect
		SpawnTrailEffect(EndPoint);

		// spawn impact effects
		SpawnImpactFX(Impact);
	}
}

bool AInstantHitWeapon::ShouldProcessDamage(AActor * TestActor) const
{
	if (TestActor != nullptr)
	{
		// if we're an actor on the server, 
		// or the actor's role is authoritative, we should register damage
		if (GetNetMode() != NM_Client ||
			TestActor->Role == ROLE_Authority ||
			TestActor->bTearOff)
		{
			return true;
		}
	}

	return false;
}

void AInstantHitWeapon::ProcessDamage(const FHitResult & Impact, const FVector & ShootDir)
{
	FPointDamageEvent PointDamageEvent;

	PointDamageEvent.DamageTypeClass = InstantWeaponConfig.DamageType;
	PointDamageEvent.HitInfo = Impact;
	PointDamageEvent.ShotDirection = ShootDir;
	PointDamageEvent.Damage = InstantWeaponConfig.HitDamage;

	Impact.GetActor()->TakeDamage(PointDamageEvent.Damage, PointDamageEvent, PawnOwner->Controller, this);
}

void AInstantHitWeapon::PlayInstantHitFX(const FVector & Origin, int32 RandomSeed, float ReticleSpead)
{
	FRandomStream WeaponRandomStream(RandomSeed);
	const float ConeHalfAngle = FMath::DegreesToRadians(ReticleSpead * 0.5f);

	const FVector AimDir = GetAdjustedAim();
	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
	const FVector EndTrace = StartTrace + ShootDir * InstantWeaponConfig.WeaponRange;

	FHitResult Impact = GetWeaponHit(StartTrace, EndTrace);
	if (Impact.bBlockingHit)
	{
		SpawnTrailEffect(Impact.ImpactPoint);
		SpawnImpactFX(Impact);
	}
	else
	{
		SpawnTrailEffect(EndTrace);
	}

}

void AInstantHitWeapon::SpawnImpactFX(const FHitResult & Impact)
{
	if (ImpactEffect && Impact.bBlockingHit)
	{
		FHitResult UseImpact = Impact;

		// trace again to find lost during replication!
		if (!Impact.Component.IsValid())
		{
			const FVector StartTrace = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;
			const FVector EndTrace = Impact.ImpactPoint - Impact.ImpactNormal * 10.0f;
			FHitResult Hit = GetWeaponHit(StartTrace, EndTrace);
			UseImpact = Hit;
		}

		const FTransform SpawnTransform(Impact.ImpactNormal.Rotation(), Impact.ImpactPoint);
		AInstantHitImpactEffect* MyImpactEffect = GetWorld()->SpawnActorDeferred<AInstantHitImpactEffect>(ImpactEffect, SpawnTransform);
		if (MyImpactEffect)
		{
			MyImpactEffect->SurfaceHit = UseImpact;
			UGameplayStatics::FinishSpawningActor(MyImpactEffect, SpawnTransform);
		}
	}
}

void AInstantHitWeapon::SpawnTrailEffect(const FVector & EndPoint)
{
	if (TrailFX)
	{
		const FVector Origin = GetMuzzleLocation();

		UParticleSystemComponent* TrailPSC = UGameplayStatics::SpawnEmitterAtLocation(this, TrailFX, Origin);
		if (TrailPSC != nullptr)
		{
			TrailPSC->SetVectorParameter(TrailTargetParam, EndPoint);
		}
	}
}

/** Helpers *********************************************************************************/

float AInstantHitWeapon::GetCurrentSpread() const
{
	float FinalSpread = InstantWeaponConfig.WeaponSpread + CurrentFiringSpread;
	if (PawnOwner && PawnOwner->IsTargeting())
	{
		FinalSpread *= InstantWeaponConfig.TargetingSpreadMod;
	}

	return FinalSpread;
}

/** Replication *****************************************************************************/

bool AInstantHitWeapon::ServerNotifyHit_Validate(const FHitResult & Impact, FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)
{
	return true;
}

void AInstantHitWeapon::ServerNotifyHit_Implementation(const FHitResult & Impact, FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)
{
	const float WeaponAngleDot = FMath::Abs(FMath::Sin(ReticleSpread * PI / 180.0f));

	// if we have an instigator, calculate the angle between shot and view
	if (Instigator != nullptr && (Impact.GetActor() || Impact.bBlockingHit))
	{
		const FVector Origin = GetMuzzleLocation();
		const FVector ViewDir = (Impact.Location - Origin).GetSafeNormal();

		// if the angle between hit and view is within allowed limits
		const float ViewDirAngleDot = FVector::DotProduct(Instigator->GetViewRotation().Vector(), ViewDir);
		if (ViewDirAngleDot > InstantWeaponConfig.AllowedHitAngleDot - WeaponAngleDot)
		{
			if (CurrentState != EWeaponState::Idle)
			{
				if (Impact.GetActor() == nullptr)
				{
					if (Impact.bBlockingHit)
					{
						ProcessInstantHit(Impact, Origin, ShootDir, RandomSeed, ReticleSpread);
					}
				}
				else if (Impact.GetActor()->IsRootComponentStatic() || Impact.GetActor()->IsRootComponentStationary())
				{
					// assume told the truth about static things because they don't move 
					// and the hit  usually doesn't have significant gameplay implications.
					ProcessInstantHit(Impact, Origin, ShootDir, RandomSeed, ReticleSpread);
				}
				else
				{
					const FBox HitBox = Impact.GetActor()->GetComponentsBoundingBox();

					// calculate the bounding box extent and increase it by a margin
					FVector BoxExtent = (HitBox.Max - HitBox.Min) * 0.5f;
					BoxExtent *= InstantWeaponConfig.SideHitOffset;

					// avooid precision errors with really thin objects
					BoxExtent.X = FMath::Max(20.f, BoxExtent.X);
					BoxExtent.Y = FMath::Max(20.f, BoxExtent.Y);
					BoxExtent.Z = FMath::Max(20.f, BoxExtent.Z);

					// calculate the box centre
					const FVector BoxCentre = (HitBox.Min + HitBox.Max) * 0.5f;

					// are we within client tolerance?
					if (FMath::Abs(Impact.Location.Z - BoxCentre.Z) < BoxExtent.Z &&
						FMath::Abs(Impact.Location.X - BoxCentre.Z) < BoxExtent.X &&
						FMath::Abs(Impact.Location.Y - BoxCentre.Z) < BoxExtent.Y)
					{
						ProcessInstantHit(Impact, Origin, ShootDir, RandomSeed, ReticleSpread);
					}
					else
					{
						UE_LOG(LogTemp, Log, TEXT("%s Rejected client side hit of %s (outside bounding box tolerance)"), *GetNameSafe(this), *GetNameSafe(Impact.GetActor()));
					}

				}
			}
		}
		else if (ViewDirAngleDot <= InstantWeaponConfig.AllowedHitAngleDot)
		{
			UE_LOG(LogTemp, Log, TEXT("%s Rejected client side hit of %s (facing too far from the hit direction)"), *GetNameSafe(this), *GetNameSafe(Impact.GetActor()));
		}
		else
		{
			UE_LOG(LogTemp, Log, TEXT("%s Rejected client side hit of %s"), *GetNameSafe(this), *GetNameSafe(Impact.GetActor()));
		}
	}
}

bool AInstantHitWeapon::ServerNotifyMiss_Validate(FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)
{
	return true;
}

void AInstantHitWeapon::ServerNotifyMiss_Implementation(FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread)
{
	const FVector Origin = GetMuzzleLocation();

	// play FX on remote clients
	HitNotification.Origin = Origin;
	HitNotification.RandomSeed = RandomSeed;
	HitNotification.ReticleSpread = ReticleSpread;

	// play fx locally
	if (GetNetMode() != NM_DedicatedServer)
	{
		const FVector EndTrace = Origin + ShootDir * InstantWeaponConfig.WeaponRange;
		SpawnTrailEffect(EndTrace);
	}
}

void AInstantHitWeapon::OnRep_HitNotify()
{
	PlayInstantHitFX(HitNotification.Origin, HitNotification.RandomSeed, HitNotification.ReticleSpread);
}

void AInstantHitWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AInstantHitWeapon, HitNotification, COND_SkipOwner);
}

