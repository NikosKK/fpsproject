// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterWeapon.h"
#include "Player/ShooterCharacter.h"
#include "Player/FPSPlayerController.h"
#include "FPSProject.h"

// Sets default values
AShooterWeapon::AShooterWeapon()
{
	USceneComponent* const TranslationComp = CreateDefaultSubobject<USceneComponent>(TEXT("Weapon"));
	RootComponent = TranslationComp;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh1P"));
	Mesh1P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh1P->bReceivesDecals = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh1P->SetupAttachment(RootComponent);

	Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh3P"));
	Mesh3P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh3P->bReceivesDecals = false;
	Mesh3P->CastShadow = false;
	Mesh3P->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh3P->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	Mesh3P->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	Mesh3P->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	Mesh3P->SetupAttachment(RootComponent);

	bLoopedMuzzleFX = false;
	bLoopedFireAnim = false;
	bPlayingFireAnim = false;
	bIsEquipped = false;
	bWantsToFire = false;
	bPendingReload = false;
	bPendingEquip = false;
	CurrentState = EWeaponState::Idle;

	CurrentAmmo = 0;
	CurrentAmmoInClip = 0;
	BurstCounter = 0;
	LastFireTime = 0.0f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bNetUseOwnerRelevancy = true;

}

// Called when the game starts or when spawned
void AShooterWeapon::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (WeaponConfig.InitialClips > 0)
	{
		CurrentAmmo = WeaponConfig.AmmoPerClip * WeaponConfig.InitialClips;
		CurrentAmmoInClip = WeaponConfig.AmmoPerClip;
	}

	DetachWeaponFromPawn();
	
}

void AShooterWeapon::Destroyed()
{
	Super::Destroyed();

	StopFireFX();
}

void AShooterWeapon::Equip(const AShooterWeapon* LastWeapon)
{
	AttachWeaponToPawn();

	bPendingEquip = true;

	DetermineWeaponState();

	// if last weapon is valid, play animation
	if (LastWeapon != nullptr)
	{
		float Duration = PlayWeaponAnimation(EquipAnim);
		if (Duration <= 0.f)
		{
			Duration = 0.5f;
		}
		
		// equip now!
		EquipStartedTime = GetWorld()->GetTimeSeconds();
		EquipDuration = Duration;

		GetWorldTimerManager().SetTimer(TimerHandle_OnEquipFinished, this, &AShooterWeapon::OnEquiped, Duration, false);
	}
	else
	{
		OnEquiped();
	}

	if (PawnOwner != nullptr && PawnOwner->IsLocallyControlled())
	{
		PlayWeaponSound(EquipSound);
	}
}

void AShooterWeapon::OnEquiped()
{
	AttachWeaponToPawn();

	bIsEquipped = true;
	bPendingEquip = false;

	DetermineWeaponState();

	if (PawnOwner != nullptr)
	{
		if (PawnOwner->IsLocallyControlled() && CurrentAmmoInClip <= 0 && CanReload())
		{
			StartReload();
		}
	}


}

void AShooterWeapon::UnEquip()
{
	DetachWeaponFromPawn();

	bIsEquipped = false;
	
	StopFire();

	// stop reload animations if needed
	if (bPendingReload)
	{
		bPendingReload = false;
		
		StopWeaponAnimation(ReloadAnim);

		GetWorldTimerManager().ClearTimer(TimerHandle_StopReload);
		GetWorldTimerManager().ClearTimer(TimerHandle_ReloadWeapon);
	}

	if (bPendingEquip)
	{
		bPendingEquip = false;

		StopWeaponAnimation(EquipAnim);

		GetWorldTimerManager().ClearTimer(TimerHandle_OnEquipFinished);
	}

	DetermineWeaponState();
}

void AShooterWeapon::AddWeapon(AShooterCharacter* NewOwner)
{
	SetPawnOwner(NewOwner);
}

void AShooterWeapon::RemoveWeapon()
{
	if (Role == ROLE_Authority)
	{
		SetPawnOwner(nullptr);
	}

	if (IsAttached())
	{
		UnEquip();
	}
}

bool AShooterWeapon::IsEquipped() const
{
	return bIsEquipped;;
}

bool AShooterWeapon::IsAttached() const
{
	return bIsEquipped || bPendingEquip;
}

float AShooterWeapon::GetEquippedStartTime() const
{
	return EquipStartedTime;
}

float AShooterWeapon::GetEquippedDuration() const
{
	return EquipDuration;
}

void AShooterWeapon::StartFire()
{
	if (Role < ROLE_Authority)
	{
		ServerStartFire();
	}

	if (!bWantsToFire)
	{
		bWantsToFire = true;

		DetermineWeaponState();
	}

}

void AShooterWeapon::StopFire()
{
	if (Role < ROLE_Authority)
	{
		ServerStopFire();
	}

	if (bWantsToFire)
	{
		bWantsToFire = false;

		DetermineWeaponState();
	}

}

void AShooterWeapon::StartReload(bool bFromReplication)
{
	if (!bFromReplication && Role < ROLE_Authority)
	{
		ServerStartReload();
	}

	if (bFromReplication || CanReload())
	{
		bPendingReload = true;

		DetermineWeaponState();

		float AnimDuration = PlayWeaponAnimation(ReloadAnim);
		if (AnimDuration <= 0.f)
		{
			AnimDuration = WeaponConfig.NoAnimReloadDuration;
		}

		GetWorldTimerManager().SetTimer(TimerHandle_StopReload, this, &AShooterWeapon::StopReload, AnimDuration, false);
		if (Role == ROLE_Authority)
		{
			GetWorldTimerManager().SetTimer(TimerHandle_ReloadWeapon, this, &AShooterWeapon::ReloadWeapon, FMath::Max(0.1f, AnimDuration - 0.1f), false);
		}

		if (PawnOwner && PawnOwner->IsLocallyControlled())
		{
			PlayWeaponSound(ReloadSound);
		}
	}
}

void AShooterWeapon::StopReload()
{
	if (CurrentState == EWeaponState::Reloading)
	{
		bPendingReload = false;
		DetermineWeaponState();
		StopWeaponAnimation(ReloadAnim);
	}
}

void AShooterWeapon::ReloadWeapon()
{
	int ClipDelta = FMath::Min(WeaponConfig.AmmoPerClip - CurrentAmmoInClip, CurrentAmmo - CurrentAmmoInClip);

	if (HasInfiniteClip())
	{
		ClipDelta = WeaponConfig.AmmoPerClip - CurrentAmmoInClip;
	}

	if (ClipDelta > 0)
	{
		CurrentAmmoInClip += ClipDelta;
	}

	if (HasInfiniteClip())
	{
		CurrentAmmo = FMath::Max(CurrentAmmoInClip, CurrentAmmo);
	}
}

bool AShooterWeapon::CanFire() const
{
	bool bCanFire = (PawnOwner != nullptr && PawnOwner->CanFire());
	bool bIsValidState = (CurrentState == EWeaponState::Idle || CurrentState == EWeaponState::Firing);
	return (bCanFire && bIsValidState && !bPendingReload);
}

bool AShooterWeapon::CanReload() const
{
	bool bCanReload = (PawnOwner != nullptr && PawnOwner->CanReload());
	bool bAmmoCheck = (CurrentAmmoInClip < WeaponConfig.AmmoPerClip && (CurrentAmmo - CurrentAmmoInClip > 0 || HasInfiniteClip()));
	bool bIsValidState = (CurrentState == EWeaponState::Idle || CurrentState == EWeaponState::Firing);
	return (bCanReload && bIsValidState && bAmmoCheck);

}

EWeaponState::Type AShooterWeapon::GetCurrentState() const
{
	return CurrentState;
}

void AShooterWeapon::GiveAmmo(int Amount)
{
	const int32 MissingAmmo = FMath::Max(0, WeaponConfig.MaxAmmo - CurrentAmmo);
	CurrentAmmo += FMath::Min(Amount, MissingAmmo);

	//AShooterAIController ...

	// start reload if clip was empty
	if (GetCurrentClipAmmo() <= 0 && CanReload() && PawnOwner->GetCurrentWeapon() == this)
	{
		ClientStartReload();
	}
}

void AShooterWeapon::UseAmmo()
{
	if (!HasInfiniteAmmo())
	{
		CurrentAmmoInClip--;
	}

	if (!HasInfiniteAmmo() && !HasInfiniteClip())
	{
		CurrentAmmo--;
	}

	// ... AI, bots etc ..==>
}

int32 AShooterWeapon::GetCurrentAmmo() const
{
	return CurrentAmmo;
}

int32 AShooterWeapon::GetCurrentClipAmmo() const
{
	return CurrentAmmoInClip;
}

int32 AShooterWeapon::GetClipSize() const
{
	return WeaponConfig.AmmoPerClip;
}

int32 AShooterWeapon::GetMaxAmmo() const
{
	return WeaponConfig.MaxAmmo;
}

bool AShooterWeapon::HasInfiniteAmmo() const
{
	//const AFPSPlayerController* PC = (PawnOwner != nullptr ? Cast<const AFPSPlayerController>(PawnOwner->Controller) : nullptr);
	//return WeaponConfig.bInfiniteAmmo || PC->HasInfiniteAmmo());
	return WeaponConfig.bInfiniteAmmo;
}

bool AShooterWeapon::HasInfiniteClip() const
{
	//const AFPSPlayerController* PC = (PawnOwner != nullptr ? Cast<const AFPSPlayerController>(PawnOwner->Controller) : nullptr);
	//return WeaponConfig.bInfiniteClip || PC->HasInfiniteClip());
	return WeaponConfig.bInfiniteClip;

}

USkeletalMeshComponent * AShooterWeapon::GetWeaponMesh() const
{
	return ((PawnOwner != nullptr && PawnOwner->IsFirstPerson()) ? Mesh1P : Mesh3P);
}

AShooterCharacter * AShooterWeapon::GetPawnOwner() const
{
	return PawnOwner;
}

void AShooterWeapon::SetPawnOwner(AShooterCharacter* NewOwner)
{
	if (PawnOwner != NewOwner)
	{
		Instigator = NewOwner;
		PawnOwner = NewOwner;
		// net owner for RPC calls
		SetOwner(NewOwner);
	}
}

void AShooterWeapon::SetWeaponState(EWeaponState::Type NewState)
{
	const EWeaponState::Type PrevState = CurrentState;

	if (PrevState == EWeaponState::Firing && NewState != EWeaponState::Firing)
	{
		OnBurstFinished();
	}

	CurrentState = NewState;

	if (PrevState != EWeaponState::Firing && NewState == EWeaponState::Firing)
	{
		OnBurstStarted();
	}
}

void AShooterWeapon::DetermineWeaponState()
{
	EWeaponState::Type NewState = EWeaponState::Idle;

	if (bIsEquipped)
	{
		if (bPendingReload)
		{
			if (CanReload())
			{
				NewState = EWeaponState::Reloading;
			}
			else
			{
				NewState = CurrentState;
			}
		}
		else if (!bPendingReload && bWantsToFire && CanFire())
		{
			NewState = EWeaponState::Firing;
		}
	}
	else if (bPendingEquip)
	{
		NewState = EWeaponState::Equipping;
	}

	SetWeaponState(NewState);
}

void AShooterWeapon::AttachWeaponToPawn()
{
	if (PawnOwner != nullptr)
	{
		// detach and hide
		DetachWeaponFromPawn();

		FName AttachPoint = PawnOwner->GetWeaponAttachPoint();

		// For locally controller players we attach both weapons
		// and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
		/*
		if (PawnOwner->IsLocallyControlled())
		{
			USkeletalMeshComponent* PawnMesh1P = PawnOwner->GetSpecificMesh(true);
			USkeletalMeshComponent* PawnMesh3P = PawnOwner->GetSpecificMesh(false);
			//
			Mesh1P->AttachToComponent(PawnMesh1P, FAttachmentTransformRules, AttachPoint);
			Mesh3P->AttachToComponent(PawnMesh3P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), AttachPoint);

			Mesh1P->SetHiddenInGame(false);
			Mesh3P->SetHiddenInGame(false);

		}
		else
		*/
		{
			USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
			USkeletalMeshComponent* UsePawnMesh = PawnOwner->GetPawnMesh();
			//UseWeaponMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::KeepRelativeTransform, AttachPoint);
			UseWeaponMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), AttachPoint);
			UseWeaponMesh->SetHiddenInGame(false);
		}
	}
}

void AShooterWeapon::DetachWeaponFromPawn()
{
	Mesh1P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	Mesh1P->SetHiddenInGame(true);

	Mesh3P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	Mesh3P->SetHiddenInGame(true);
}

bool AShooterWeapon::ServerStartFire_Validate()
{
	return true;
}

void AShooterWeapon::ServerStartFire_Implementation()
{
	StartFire();
}

bool AShooterWeapon::ServerStopFire_Validate()
{
	return true;
}

void AShooterWeapon::ServerStopFire_Implementation()
{
	StopFire();
}

bool AShooterWeapon::ServerStartReload_Validate()
{
	return true;
}

void AShooterWeapon::ServerStartReload_Implementation()
{
	StartReload();
}

void AShooterWeapon::ClientStartReload_Implementation()
{
	StartReload();
}

bool AShooterWeapon::ServerStopReload_Validate()
{
	return true;
}

void AShooterWeapon::ServerStopReload_Implementation()
{
	StartReload();
}

bool AShooterWeapon::ServerHandleFiring_Validate()
{
	return true;
}

void AShooterWeapon::ServerHandleFiring_Implementation()
{
	const bool bShouldUpdateAmmo = (CurrentAmmoInClip > 0 && CanFire());

	HandleFiring();

	if (bShouldUpdateAmmo)
	{
		// consume ammo
		UseAmmo();

		// update fx on remote clients
		BurstCounter++;
	}
}


void AShooterWeapon::HandleFiring()
{
	if ((CurrentAmmoInClip > 0 || HasInfiniteClip() || HasInfiniteAmmo()) && CanFire())
	{
		if (GetNetMode() != NM_DedicatedServer)
		{
			StartFireFX();
		}

		if (PawnOwner && PawnOwner->IsLocallyControlled())
		{
			// specific weapon fire method
			FireWeapon();

			// consume ammo
			UseAmmo();
			
			// update fx on remote clients
			BurstCounter++;
		}
	}
	else if (CanReload())
	{
		StartReload();
	}
	else if (PawnOwner && PawnOwner->IsLocallyControlled())
	{
		if (GetCurrentAmmo() == 0 && !bRefiring)
		{
			PlayWeaponSound(OutOfAmmoSound);

			AFPSPlayerController* PC = Cast<AFPSPlayerController>(PawnOwner->Controller);
			// notify hud ===> ....
		}

		if (BurstCounter > 0)
		{
			OnBurstFinished();
		}
	}

	if (PawnOwner && PawnOwner->IsLocallyControlled())
	{
		if (Role < ROLE_Authority)
		{
			// local client will notify server
			ServerHandleFiring();
		}

		// reload if needed
		if (CurrentAmmoInClip <= 0 && CanReload())
		{
			StartReload();
		}

		// setup refire timer
		bRefiring = (CurrentState == EWeaponState::Firing && WeaponConfig.TimeBetweenShots > 0.f);
		if (bRefiring)
		{
			GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &AShooterWeapon::HandleFiring, WeaponConfig.TimeBetweenShots, false);
		}
	}

	// save fire frame time
	LastFireTime = GetWorld()->GetTimeSeconds();
}

void AShooterWeapon::OnBurstStarted()
{
	const float Now = GetWorld()->GetTimeSeconds();
	const float FireRate = WeaponConfig.TimeBetweenShots;

	// start firing, can be delayed to satisfy TimeBetweenShots
	if (LastFireTime > 0 && FireRate > 0.f && LastFireTime + FireRate > Now)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_HandleFiring, this, &AShooterWeapon::HandleFiring, LastFireTime + FireRate - Now, false);
	}
	else
	{
		HandleFiring();
	}
}

void AShooterWeapon::OnBurstFinished()
{
	// stop fx on remote clients
	BurstCounter = 0;

	// stop fx locally, unless is a dedicated server
	if (GetNetMode() != NM_DedicatedServer)
	{
		StopFireFX();
	}

	GetWorldTimerManager().ClearTimer(TimerHandle_HandleFiring);
	bRefiring = false;

}

void AShooterWeapon::StartFireFX()
{
	if (Role == ROLE_Authority && CurrentState != EWeaponState::Firing)
	{
		return;
	}

	if (MuzzleFX)
	{		
		if (!bLoopedMuzzleFX || MuzzlePSC == nullptr)
		{
			if (PawnOwner && PawnOwner->IsLocallyControlled())
			{
				// Split screen requires we create 2 effects. 
				// One that we see and one that the other player sees.
				AController* PController = PawnOwner->GetController();
				if (PController != nullptr)
				{
					Mesh1P->GetSocketLocation(MuzzleAttachPoint);
					MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, Mesh1P, MuzzleAttachPoint);
					MuzzlePSC->bOwnerNoSee = false;
					MuzzlePSC->bOnlyOwnerSee = true;

					Mesh3P->GetSocketLocation(MuzzleAttachPoint);
					MuzzlePSCSecondary = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, Mesh3P, MuzzleAttachPoint);
					MuzzlePSCSecondary->bOwnerNoSee = true;
					MuzzlePSCSecondary->bOnlyOwnerSee = false;

				}
			}
			else
			{
				USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
				MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, UseWeaponMesh, MuzzleAttachPoint);
			}
		}
	}

	if (!bLoopedFireAnim || !bPlayingFireAnim)
	{
		bPlayingFireAnim = true;
		PlayWeaponAnimation(FireAnim);
	}

	if (bLoopedFireSound)
	{
		if (FireAC == nullptr)
		{
			FireAC = PlayWeaponSound(FireLoopSound);
		}
	}
	else
	{
		PlayWeaponSound(FireSound);
	}

	AFPSPlayerController* PC = (PawnOwner ? Cast<AFPSPlayerController>(PawnOwner->Controller) : nullptr);
	if (PC && PC->IsLocalPlayerController())
	{
		if (FireCameraShake != nullptr)
		{
			PC->ClientPlayCameraShake(FireCameraShake, 1);
		}

		if (FireForceFeedback != nullptr)
		{
			PC->ClientPlayForceFeedback(FireForceFeedback, false, false, "Weapon");
		}
	}
}

void AShooterWeapon::StopFireFX()
{
	if (bLoopedMuzzleFX)
	{
		if (MuzzlePSC != nullptr)
		{
			MuzzlePSC->DeactivateSystem();
			MuzzlePSC = nullptr;
		}

		if (MuzzlePSCSecondary != nullptr)
		{
			MuzzlePSCSecondary->DeactivateSystem();
			MuzzlePSCSecondary = nullptr;
		}
	}

	if (bLoopedFireAnim && bPlayingFireAnim)
	{
		bPlayingFireAnim = false;
		StopWeaponAnimation(FireAnim);
	}

	if (FireAC)
	{
		FireAC->FadeOut(0.1f, 0.f);
		FireAC = nullptr;

		PlayWeaponSound(FireFinishSound);
	}
}

UAudioComponent * AShooterWeapon::PlayWeaponSound(USoundCue * Sound)
{
	UAudioComponent* AC = nullptr;
	if (Sound && PawnOwner)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, PawnOwner->GetRootComponent());
	}

	return AC;
}

float AShooterWeapon::PlayWeaponAnimation(const FWeaponAnim & Animation)
{
	float Duration = 0.0f;

	if (PawnOwner)
	{
		UAnimMontage* UseAnim = PawnOwner->IsFirstPerson() ? Animation.Pawn1P : Animation.Pawn3P;
		if (UseAnim)
		{
			Duration = PawnOwner->PlayAnimMontage(UseAnim);
		}
	}

	return Duration;
}

void AShooterWeapon::StopWeaponAnimation(const FWeaponAnim & Animation)
{
	if (PawnOwner != nullptr)
	{
		UAnimMontage* UseAnim = PawnOwner->IsFirstPerson() ? Animation.Pawn1P : Animation.Pawn3P;
		if (UseAnim)
		{
			PawnOwner->StopAnimMontage(UseAnim);
		}
	}
}

FVector AShooterWeapon::GetAdjustedAim() const
{
	AFPSPlayerController* const PC = Instigator ? Cast<AFPSPlayerController>(Instigator->Controller) : nullptr;
	FVector FinalAim = FVector::ZeroVector;

	if (PC)
	{
		FVector CameraLocation;
		FRotator CameraRotation;
		PC->GetPlayerViewPoint(CameraLocation, CameraRotation);
		FinalAim = CameraRotation.Vector();
	}
	else if (Instigator)
	{
		// ...AI
	}

	return FinalAim;
}

FVector AShooterWeapon::GetCameraAim() const
{
	AFPSPlayerController* const PC = Instigator ? Cast<AFPSPlayerController>(Instigator->Controller) : nullptr;
	FVector FinalAim = FVector::ZeroVector;

	if (PC)
	{
		FVector CameraLocation;
		FRotator CameraRotation;
		PC->GetPlayerViewPoint(CameraLocation, CameraRotation);
		FinalAim = CameraRotation.Vector();
	}
	else if (Instigator)
	{
		FinalAim = Instigator->GetBaseAimRotation().Vector();
	}

	return FinalAim;

}

FVector AShooterWeapon::GetCameraDamageStartLocation(const FVector & AimDir) const
{
	AFPSPlayerController* PC = Cast<AFPSPlayerController>(PawnOwner->Controller);
	FVector OutStartTrace = FVector::ZeroVector;

	if (PC)
	{
		// use player camera
		FRotator Rotation;
		PC->GetPlayerViewPoint(OutStartTrace, Rotation);

		// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
		OutStartTrace = OutStartTrace + AimDir * (FVector::DotProduct((Instigator->GetActorLocation() - OutStartTrace), AimDir));
	}
	else
	{
		OutStartTrace = GetMuzzleLocation();
	}
	
	return OutStartTrace;
}

FVector AShooterWeapon::GetMuzzleLocation() const
{
	USkeletalMeshComponent* UseMesh = GetWeaponMesh();
	return UseMesh->GetSocketLocation(MuzzleAttachPoint);
}

FVector AShooterWeapon::GetMuzzleDirection() const
{
	USkeletalMeshComponent* UseMesh = GetWeaponMesh();
	return UseMesh->GetSocketRotation(MuzzleAttachPoint).Vector();

}

FHitResult AShooterWeapon::GetWeaponHit(const FVector & StartTrace, const FVector & EndTrace) const
{
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));

	// perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult HitResult(ForceInit);
	GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, COLLISION_WEAPON, TraceParams);

	return HitResult;
}

void AShooterWeapon::OnRep_PawnOwner()
{
	if (PawnOwner)
	{
		AddWeapon(PawnOwner);
	}
	else
	{
		RemoveWeapon();
	}
}

void AShooterWeapon::OnRep_BurstCounter()
{
	if (BurstCounter > 0)
	{
		StartFireFX();
	}
	else
	{
		StopFireFX();
	}
}

void AShooterWeapon::OnRep_Reload()
{
	if (bPendingReload)
	{
		StartReload(true);
	}
	else
	{
		StopReload();
	}
}

void AShooterWeapon::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AShooterWeapon, PawnOwner);

	DOREPLIFETIME_CONDITION(AShooterWeapon, CurrentAmmo, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AShooterWeapon, CurrentAmmoInClip, COND_OwnerOnly);

	DOREPLIFETIME_CONDITION(AShooterWeapon, BurstCounter, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AShooterWeapon, bPendingReload, COND_SkipOwner);
}
