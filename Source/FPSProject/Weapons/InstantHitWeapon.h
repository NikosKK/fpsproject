// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/ShooterWeapon.h"
#include "InstantHitWeapon.generated.h"

/** Hit Info record for replication */
USTRUCT()
struct FInstantHitInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FVector Origin;

	UPROPERTY()
	float ReticleSpread;

	UPROPERTY()
	int32 RandomSeed;
};

USTRUCT()
struct FInstantHitWeaponData
{
	GENERATED_USTRUCT_BODY()

	// weapon fire spread (degrees)
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float WeaponSpread;

	// targeting spread modifier
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float TargetingSpreadMod;

	// continuous firing : spread increment
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float FiringSpreadIncrement;

	// continuous firing : max spread increment
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float FiringMaxSpreadIncrement;

	// max weapon range
	UPROPERTY(EditDefaultsOnly, Category = "Stats")
	float WeaponRange;

	// damage amount
	UPROPERTY(EditDefaultsOnly, Category = "Stats")
	int32 HitDamage;

	// damage type
	UPROPERTY(EditDefaultsOnly, Category = "Stats")
	TSubclassOf<UDamageType> DamageType;

	// scale for bounding box of hit actor
	UPROPERTY(EditDefaultsOnly, Category = "Network Hit")
	float SideHitOffset;

	// threshold for dot product between view and hit directions
	UPROPERTY(EditDefaultsOnly, Category = "Network Hit")
	float AllowedHitAngleDot;

	FInstantHitWeaponData()
	{
		WeaponSpread = 5.f;
		TargetingSpreadMod = 0.1f;
		FiringSpreadIncrement = 1.f;
		FiringMaxSpreadIncrement = 5.f;
		WeaponRange = 10000.f;
		HitDamage = 10;
		DamageType = UDamageType::StaticClass();
		SideHitOffset = 200.f;
		AllowedHitAngleDot = 0.8f;
	}

};

/**
 * 
 */
UCLASS(Abstract)
class FPSPROJECT_API AInstantHitWeapon : public AShooterWeapon
{
	GENERATED_BODY()
	
public:

	AInstantHitWeapon();

	// get current spread
	float GetCurrentSpread() const;

protected:

	virtual EAmmoType GetAmmoType() const override { return EAmmoType::EBullet; }

	// specific weapon fire implementation
	virtual void FireWeapon() override;

	// [local + server] update fire spread
	virtual void OnBurstFinished() override;

protected:

	// handle the instant hit and notify the server if necessary
	void HandleInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread);
	
	// process the instant hit as if it has been confirmed by the server
	void ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread);

	// check if the weapon should deal damage to the hit actor
	bool ShouldProcessDamage(AActor* TestActor) const;

	// handle damage
	void ProcessDamage(const FHitResult& Impact, const FVector& ShootDir);

	// called in network play to play cosmetic fx
	void PlayInstantHitFX(const FVector& Origin, int32 RandomSeed, float ReticleSpead);

	// spawn impact effects
	void SpawnImpactFX(const FHitResult& Impact);

	// spawn trail effect
	void SpawnTrailEffect(const FVector& EndPoint);

protected:

	UFUNCTION(reliable, server, WithValidation)
	void ServerNotifyHit(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread);
	bool ServerNotifyHit_Validate(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread);
	void ServerNotifyHit_Implementation(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread);

	UFUNCTION(reliable, server, WithValidation)
	void ServerNotifyMiss(FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread);
	bool ServerNotifyMiss_Validate(FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread);
	void ServerNotifyMiss_Implementation(FVector_NetQuantizeNormal ShootDir, int32 RandomSeed, float ReticleSpread);

	UFUNCTION()
	void OnRep_HitNotify();

protected:

	// weapon configuration
	UPROPERTY(EditDefaultsOnly, Category = "Config")
	FInstantHitWeaponData InstantWeaponConfig;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	TSubclassOf<class AInstantHitImpactEffect> ImpactEffect;

	// smoke trail
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UParticleSystem* TrailFX;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	FName TrailTargetParam;

	float CurrentFiringSpread;	

	/** instant hit notify for replication */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_HitNotify)
	FInstantHitInfo HitNotification;
	
};
