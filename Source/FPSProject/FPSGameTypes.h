// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FPSGameTypes.generated.h"

// defined in Project Settings/Physics/Surface Type
// used in weapon impact effect (could be placed there?)
#define FPSPROJECT_SURFACE_Default	SurfaceType_Default
#define FPSPROJECT_SURFACE_Concrete	SurfaceType1

/*
* Surface type enum
* (keep in sync with Project physical material surface types)
*/
UENUM()
namespace EWeaponImpactMaterialType
{
	enum Type
	{
		Unknown,
		Concrete,
	};
}

USTRUCT()
struct FDecalData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Decal)
	class UMaterial* DecalMaterial;

	UPROPERTY(EditDefaultsOnly, Category = Decal)
	float DecalSize;

	UPROPERTY(EditDefaultsOnly, Category = Decal)
	float LifeSpan;

	FDecalData() : DecalSize(256.f), LifeSpan(10.f)	{}
};

UENUM(BlueprintType)
namespace EFPSGameTeam
{
	enum Type
	{
		None,
		Player,
		Enemy,
		MAX
	};
}
