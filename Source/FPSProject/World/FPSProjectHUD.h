// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "FPSProjectHUD.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AFPSProjectHUD : public AHUD
{
	GENERATED_BODY()
	
public:

	AFPSProjectHUD();

	// Primary draw call for the HUD
	virtual void DrawHUD() override;

protected:

	// simple crosshair draw implementation
	void DrawWeaponCrosshair();

protected:

	// crosshair asset pointer
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	class UTexture2D* CrosshairTex;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FVector2D CrosshairTexOffset;
	
};
