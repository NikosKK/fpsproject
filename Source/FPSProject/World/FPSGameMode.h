// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPSGameMode.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AFPSGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	// set class defaults
	AFPSGameMode();

public:

	/** Initializes player pawn back to starting values, called from RestartPlayer */
	virtual void SetPlayerDefaults(APawn* PlayerPawn) override;

protected:

	// spawn default weapons
	virtual void SpawnAvailableWeapons(APawn* InPawn);

protected:

	// default weapon classes
	UPROPERTY(EditDefaultsOnly, Category = "Weapons")
	TArray<TSubclassOf<class AShooterWeapon>> AvailableWeapons;


	
	
	
};
