// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSGameMode.h"
#include "FPSProjectHUD.h"
#include "Player/FPSPlayerController.h"
#include "Player/ShooterCharacter.h"
#include "Weapons/ShooterWeapon.h"
#include "FPSProject.h"

AFPSGameMode::AFPSGameMode()
{
	PlayerControllerClass = AFPSPlayerController::StaticClass();
}

void AFPSGameMode::SetPlayerDefaults(APawn* PlayerPawn)
{
	Super::SetPlayerDefaults(PlayerPawn);

	SpawnAvailableWeapons(PlayerPawn);
}

void AFPSGameMode::SpawnAvailableWeapons(APawn* InPawn)
{
	AShooterCharacter* MyCharacter = Cast<AShooterCharacter>(InPawn);
	if (MyCharacter)
	{
		int32 WeaponClassCount = AvailableWeapons.Num();
		for (int32 i = 0; i < WeaponClassCount; i++)
		{
			if (AvailableWeapons[i])
			{
				FActorSpawnParameters SpawnInfo;
				SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				AShooterWeapon* NewWeapon = GetWorld()->SpawnActor<AShooterWeapon>(AvailableWeapons[i], SpawnInfo);

				MyCharacter->AddWeapon(NewWeapon);
			}
		}
	}
}
