// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProjectHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"

AFPSProjectHUD::AFPSProjectHUD()
{
	CrosshairTexOffset = FVector2D(0.0f, 20.0f);
}

void AFPSProjectHUD::DrawHUD()
{
	Super::DrawHUD();

	// draw very simple weapon crosshair
	DrawWeaponCrosshair();

}

void AFPSProjectHUD::DrawWeaponCrosshair()
{
	// find Canvas center
	const FVector2D HUDCenter(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that 
	// the centre of the texture aligns with the center of the canvas.
	const FVector2D CrosshairDrawPosition(HUDCenter.X + CrosshairTexOffset.X, HUDCenter.Y + CrosshairTexOffset.Y);

	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

